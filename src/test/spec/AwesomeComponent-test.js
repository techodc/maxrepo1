// Mocking window and document object:
require('../dom-mock')('<html><body></body></html>');

var jsdom = require('mocha-jsdom');
var assert = require('assert');
var React = require('react');
var TestUtils = require('react-addons-test-utils');

describe('Testing my div', function() {
  jsdom({ skipWindowCheck: true });

  it('should contain text', function() {
    var AwesomeComponent = require('../../client/app/index.jsx');
    var myDiv = TestUtils.renderIntoDocument(AwesomeComponent);
    var divText = TestUtils.findRenderedDOMComponentWithTag(myDiv, 'span');

    assert.equal(divText.textContent, 'Hello React! How are you doing?');
  });
});
