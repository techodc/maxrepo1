import React from 'react';
import {render} from 'react-dom';

import AwesomeComponent from './AwesomeComponent.jsx';

class App extends React.Component {
  render () {
    return (
    <div>
      <span>Hello React! How are you doing?</span>
      <AwesomeComponent />
    </div>);
  }
}

render(<App/>, document.getElementById('app'));
